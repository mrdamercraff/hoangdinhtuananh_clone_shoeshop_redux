import logo from "./logo.svg";
import "./App.css";
import Clone_ShoeShop from "./Clone_ShoeShop_Redux/Clone_ShoeShop";

function App() {
  return (
    <div className="App">
      <Clone_ShoeShop />
    </div>
  );
}

export default App;
