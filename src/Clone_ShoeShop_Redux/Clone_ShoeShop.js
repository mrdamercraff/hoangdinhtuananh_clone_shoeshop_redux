import React, { Component } from "react";
import Cart from "./Cart";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Clone_ShoeShop extends Component {
  render() {
    return (
      <div>
        <Cart />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
