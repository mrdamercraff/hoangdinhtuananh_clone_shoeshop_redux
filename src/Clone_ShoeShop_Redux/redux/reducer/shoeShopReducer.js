import { dataShoe } from "../../dataShoe";
import {
  ADD_TO_CART,
  CHANGE_DETAIL,
  GIAM_SO_LUONG,
  TANG_SO_LUONG,
} from "./constReducer/shoeConstant";

let innitialState = {
  shoeArr: dataShoe,
  detail: [],
  Cart: [],
};
export const shoeReducer = (state = innitialState, action) => {
  switch (action.type) {
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }
    case ADD_TO_CART: {
      let cloneCart = [...state.Cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }

      return { ...state, Cart: cloneCart };
    }
    case TANG_SO_LUONG: {
      let cloneCart = [...state.Cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        return;
      } else {
        cloneCart[index].number++;
      }
      return { ...state, Cart: cloneCart };
    }
    case GIAM_SO_LUONG: {
      let cloneCart = [...state.Cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        return;
      } else {
        if (cloneCart[index].number == 1) {
          cloneCart.splice(index, 1);
        } else {
          cloneCart[index].number--;
        }
      }
      return { ...state, Cart: cloneCart };
    }

    default:
      return state;
  }
};
