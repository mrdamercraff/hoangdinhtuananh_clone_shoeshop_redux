import { combineReducers } from "redux";
import { shoeReducer } from "./shoeShopReducer";
export const rootReducer_Shoe_Shop = combineReducers({ shoeReducer });
