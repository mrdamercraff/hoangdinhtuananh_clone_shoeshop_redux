import React, { Component } from "react";
import { connect } from "react-redux";
import {
  GIAM_SO_LUONG,
  TANG_SO_LUONG,
} from "./redux/reducer/constReducer/shoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td style={{ fontSize: "20px" }}>{item.id}</td>
          <td style={{ fontSize: "20px" }}>{item.name}</td>
          <td style={{ fontSize: "20px" }}>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleGiamSoLuong(item);
              }}
              className="btn btn-danger mr-2"
            >
              GIAM
            </button>
            <span style={{ fontSize: "20px" }}>{item.number}</span>
            <button
              onClick={() => {
                this.props.handleTangSoLuong(item);
              }}
              className="btn btn-warning ml-2"
            >
              TANG
            </button>
          </td>

          <td>
            <img style={{ width: "90px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>PRICE</th>
            <th>QUANTITY</th>
            <th>IMAGE</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.Cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: (shoe) => {
      let action = {
        type: TANG_SO_LUONG,
        payload: shoe,
      };
      dispatch(action);
    },
    handleGiamSoLuong: (shoe) => {
      let action = {
        type: GIAM_SO_LUONG,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
